// Try VRt Contest 2019 (marathon)
// author: Leonardone @ NEETSDKASU
import std.datetime.stopwatch : AutoStart, StopWatch;
import std.stdio : File;

void test(bool ana)
{
    import std.file : exists;
    import std.format : format;
    import std.stdio : stderr;

    auto sw = StopWatch(AutoStart.yes);

    foreach (ti; 1 .. 11)
    {
        stderr.writeln("-----------------");
        stderr.writeln("seed: ", ti);
        auto f = File(r"tests\" ~ "%03d.txt".format(ti), "r");
        scope(exit) f.close();
        sw.reset;
        auto problem = new Problem(f);
        if (ana)
        {
            auto an = "%03d_ana.txt".format(ti);
            if (!an.exists)
            {
                auto a = File(an, "w");
                scope(exit) a.close();
                analyze(a, problem);
            }
            auto an2 = "%03d_ana2.txt".format(ti);
            if (!an2.exists)
            {
                auto a2 = File(an2, "w");
                scope(exit) a2.close();
                analyze2(a2, problem);
            }
            continue;
        }
        auto solution = solve(problem, sw);
        auto workers = problem.fix(solution.workers);
        problem.validate(workers);
        problem.calcScore(workers);
    }
}

void main(string[] args)
{
    import std.stdio : stdin, stdout;

    debug
    {
        if (args.length < 3)
        {
            test(args.length == 2);
            return;
        }
    }

    auto sw = StopWatch(AutoStart.yes);

    auto problem = new Problem(stdin);

    auto solution = solve(problem, sw);

    auto workers = problem.fix(solution.workers);

    debug
    {
        problem.validate(workers);
        problem.calcScore(workers);
    }

    output(stdout, workers);
}

class Pos
{
    const int x, y;
    this(int[] s)
    {
        x = s[0];
        y = s[1];
    }

    int distance(const Pos other) const
    {
        import std.math : abs;

        return abs(x - other.x) + abs(y - other.y);
    }
}

class Location
{
    const int id;
    const Pos pos;
    const int duration;
    const int require;
    const int startTime;
    const int endTime;
    this(int id, int[] s)
    {
        this.id = id;
        pos = new Pos(s);
        duration = s[2];
        require = s[3];
        startTime = s[4];
        endTime = s[5];
    }

}

class Problem
{
    const int n;
    const Location base;
    Location[] locations;
    this(File f)
    {
        import std.algorithm : map, splitter;
        import std.array : array;
        import std.conv : to;
        import std.string : chomp;

        n = f.readln.chomp.to!int;
        locations = new Location[n - 1];
        auto s = f.readln.chomp.splitter.map!(to!int).array;
        base = new Location(1, s);
        foreach (i; 1 .. n)
        {
            s = f.readln.chomp.splitter.map!(to!int).array;
            locations[i - 1] = new Location(i + 1, s);
        }
    }

    auto get(int id)
    {
        return locations[id - 2];
    }
}

enum CommandType
{
    START,
    ARRIVE,
    WORK,
    END
}

class Command
{
    static Command start(int t1)
    {
        return new Command(CommandType.START, t1, t1, 1);
    }

    static Command arrive(int t1, int id = 1)
    {
        return new Command(CommandType.ARRIVE, t1, t1, id);
    }

    static Command work(int t1, int t2, int id)
    {
        return new Command(CommandType.WORK, t1, t2, id);
    }

    static Command end()
    {
        return new Command(CommandType.END, 0, 0, 1);
    }

    const CommandType ctype;
    const int time1, time2, id;
    this(CommandType ct, int t1, int t2, int id)
    {
        ctype = ct;
        time1 = t1;
        time2 = t2;
        this.id = id;
    }

    override string toString() const
    {
        import std.conv : text;

        final switch (ctype)
        {
        case CommandType.START:
            return text("start ", time1, " 1");
        case CommandType.ARRIVE:
            return text("arrive ", time1, ' ', id);
        case CommandType.WORK:
            return text("work ", time1, ' ', time2, ' ', id);
        case CommandType.END:
            return "end";
        }
    }
}

class Worker
{
    import std.container.dlist : DList;

    DList!Command list;
    alias list this;
}

auto fix(Problem p, Worker[] workers)
{
    import std.array : appender;

    auto ret = appender!(Worker[]);
    foreach (w; workers)
    {
        if (w.empty)
        {
            continue;
        }
        if (w.front.ctype != CommandType.START)
        {
            auto start = Command.start(w.front.time1 - p.get(w.front.id).pos.distance(p.base.pos));
            w.insertFront(start);
        }
        if (w.back.ctype != CommandType.END)
        {
            if (w.back.ctype != CommandType.ARRIVE || w.back.id != 1)
            {
                auto back = Command.arrive(w.back.time2 + p.get(w.back.id).pos.distance(p.base.pos));
                w.insertBack(back);
            }
            w.insertBack(Command.end);
        }
        ret.put(w);
    }
    return ret.data[];
}

void output(File f, Worker[] workers)
{
    foreach (w; workers)
    {
        foreach (c; w[])
        {
            f.writeln(c);
        }
    }
}

void validate(Problem p, Worker[] ws)
{
    auto times = new int[p.n + 1];
    auto count = new int[p.n + 1];
    foreach (w; ws)
    {
        p.validateWorker(w, times, count);
    }
    foreach (i; 2 .. p.n + 1)
    {
        assert((times[i] == 0 && count[i] == 0) || (times[i] > 0 && count[i] == p.get(i).require));
    }
}

void validateWorker(Problem p, Worker w, int[] times, int[] count)
{
    assert(w !is null);
    assert(!w.empty);

    auto startFlag = false;
    auto endFlag = false;

    auto lastId = 0;
    auto lastTime = 0;

    foreach (c; w)
    {
        assert(endFlag == false);

        final switch (c.ctype)
        {
        case CommandType.START:
            assert(startFlag == false);
            assert(0 <= c.time1 && c.time1 <= 1000);
            assert(c.id == 1);
            startFlag = true;
            lastId = 1;
            lastTime = c.time1;
            break;

        case CommandType.ARRIVE:
            assert(startFlag);
            assert(0 <= c.time1 && c.time1 <= 1000);
            assert(1 <= c.id && c.id <= p.n);
            auto pFrom = (lastId == 1 ? p.base : p.get(lastId)).pos;
            auto pTo = (c.id == 1 ? p.base : p.get(c.id)).pos;
            auto dist = pFrom.distance(pTo);
            assert(lastTime + dist <= c.time1);
            lastId = c.id;
            lastTime = c.time1;
            break;

        case CommandType.WORK:
            assert(startFlag);
            assert(0 <= c.time1 && c.time1 <= 1000);
            assert(0 <= c.time2 && c.time2 <= 1000);
            assert(lastTime <= c.time1);
            assert(1 < c.id && c.id <= p.n);
            assert(c.id == lastId);
            auto loc = p.get(c.id);
            assert(loc.startTime <= c.time1);
            assert(c.time1 + loc.duration == c.time2);
            assert(c.time2 <= loc.endTime);
            assert(times[c.id] == 0 || times[c.id] == c.time1);
            times[c.id] = c.time1;
            ++count[c.id];
            assert(count[c.id] <= loc.require);
            lastTime = c.time2;
            break;

        case CommandType.END:
            assert(startFlag);
            assert(lastId == 1);
            endFlag = true;
            break;
        }
    }

    assert(endFlag);
}

void calcScore(Problem p, Worker[] ws)
{
    import std.stdio : stderr;

    auto flag = new bool[p.n + 1];
    auto time = 0;
    auto countW = 0;
    foreach (w; ws)
    {
        ++countW;
        auto last = 0;
        foreach (c; w)
        {
            if (c.ctype == CommandType.ARRIVE)
            {
                last = c.time1;
            }
            if (c.ctype == CommandType.WORK)
            {
                flag[c.id] = true;
            }
        }
        time += last - w.front.time1;
    }
    auto countL = 0;
    auto earn = 0;
    auto duration = 0;
    foreach (i, f; flag)
    {
        if (f)
        {
            ++countL;
            auto loc = p.get(i);
            earn += loc.duration * loc.require * (loc.require + 5);
            duration += loc.duration * loc.require;
        }
    }
    auto cost = time + 240 * countW;
    auto realScore = earn - cost;
    auto score = realScore < 0 ? 0.0 : double(realScore) / 1000.0;
    stderr.writeln("alllocation: ", p.n);
    stderr.writeln("location: ", countL);
    stderr.writeln("workers: ", countW);
    stderr.writeln("earn: ", earn);
    stderr.writeln("cost: ", cost);
    stderr.writeln("timecost: ", time);
    stderr.writeln("movecost: ", time - duration);
    stderr.writeln("workcost: ", duration);
    if (countW) stderr.writeln("averagetime: ", time / countW);
    stderr.writeln("realScore: ", realScore);
    stderr.writeln("Score = ", score);
}

void analyze(File f, Problem p)
{
    import std.algorithm : count, maxElement, sort;
    import std.array : array;
    import std.conv : text;
    import std.range : repeat;

    auto countW = new int[1001];
    auto countL = new int[1001];
    auto pos = new int[101 * 101];
    auto dur = new int[31];
    auto req = new int[8];
    auto interval = new int[301];
    foreach (loc; p.locations)
    {
        ++dur[loc.duration];
        ++req[loc.require];
        ++pos[loc.pos.y * 101 + loc.pos.x];
        ++interval[loc.endTime - loc.startTime];
        foreach (t; loc.startTime .. loc.endTime + 1)
        {
            countW[t] += loc.require;
            ++countL[t];
        }
    }

    f.writeln("dup count: ", pos.count!"a > b"(1));
    f.writeln("max dup: ", pos.maxElement);
    f.writeln("max worker: ", countW.maxElement);
    f.writeln("max work: ", countL.maxElement);
    f.writeln;
    f.writeln;
    f.writeln("requires");
    foreach (i; 1 .. 8)
    {
        f.writeln(i, ": ", req[i]);
    }
    f.writeln("durations");
    foreach (i; 5 .. 31)
    {
        f.writefln("%2d: %d", i, dur[i]);
    }
    f.writeln;
    f.writeln;

    foreach (i; 200 .. 801)
    {
        f.writef("%4d,", i);
    }
    f.writeln;
    foreach (c; countW[200 .. 801])
    {
        f.writef("%4d,", c);
    }
    f.writeln;
    foreach (c; countL[200 .. 801])
    {
        f.writef("%4d,", c);
    }

    f.writeln;
    f.writeln;
    f.writefln("base: (%3d, %3d)", p.base.pos.x, p.base.pos.y);
    auto locs = p.locations.dup;
    locs.sort!"a.startTime < b.startTime";
    foreach (loc; locs)
    {
        f.writef("%4d: (%3d, %3d): dur(%3d): req(%3d): st(%3d): ed(%3d): int(%3d):",
            loc.id, loc.pos.x, loc.pos.y, loc.duration, loc.require,
            loc.startTime, loc.endTime, loc.endTime - loc.startTime);
        f.write('-'.repeat(loc.startTime - 200).array);
        f.writeln('*'.repeat(loc.endTime - loc.startTime).array);
    }

    f.writeln;
    f.writeln;
    auto line = new char[101];
    foreach (y; 0 .. 101)
    {
        foreach (x; 0 .. 101)
        {
            if (pos[y * 101 + x] == 0)
            {
                if (p.base.pos.x == x && p.base.pos.y == y)
                {
                    line[x] = 'B';
                }
                else
                {
                    line[x] = '.';
                }
            }
            else if (p.base.pos.x == x && p.base.pos.y == y)
            {
                line[x] = 'D';
            }
            else if (pos[y * 101 + x] == 1)
            {
                line[x] = '*';
            }
            else
            {
                line[x] = pos[y * 101 + x].text[0];
            }
        }
        f.writeln(line);
    }

    f.writeln;
    f.writeln;
    f.writeln("endTime order");
    locs.sort!"a.endTime < b.endTime";
    foreach (loc; locs)
    {
        f.writef("%4d: (%3d, %3d): dur(%3d): req(%3d): st(%3d): ed(%3d): int(%3d):",
            loc.id, loc.pos.x, loc.pos.y, loc.duration, loc.require,
            loc.startTime, loc.endTime, loc.endTime - loc.startTime);
        f.write('-'.repeat(loc.startTime - 200).array);
        f.writeln('*'.repeat(loc.endTime - loc.startTime).array);
    }

    f.writeln;
    f.writeln;
    f.writeln("intervals");
    foreach (i; 60 .. 301)
    {
        f.writefln("%3d: %d", i, interval[i]);
    }

    f.writeln;
    f.writeln;
    f.writeln("interval order");
    locs.sort!"a.endTime - a.startTime < b.endTime - b.startTime";
    foreach (loc; locs)
    {
        f.writef("%4d: (%3d, %3d): dur(%3d): req(%3d): st(%3d): ed(%3d): int(%3d):",
            loc.id, loc.pos.x, loc.pos.y, loc.duration, loc.require,
            loc.startTime, loc.endTime, loc.endTime - loc.startTime);
        f.write('-'.repeat(loc.startTime - 200).array);
        f.writeln('*'.repeat(loc.endTime - loc.startTime).array);
    }
}

void analyze2(File f, Problem p)
{
    import std.algorithm : max, min;
    import std.array : array;
    import std.range : repeat;
    import std.stdio : stderr;

    auto sol0 = p.solveG(1000);
    auto maxW = sol0.countW;

    auto cws = new Solution[maxW + 1];
    sol0.workers = null;
    cws[maxW] = sol0;

    auto maxScore = sol0.score;
    auto minScore = sol0.score;

    foreach (cw; 1 .. maxW)
    {
        auto sol = p.solveG(cw);
        sol.workers = null;
        cws[cw] = sol;
        maxScore = max(maxScore, sol.score);
        if (sol.score * 10 <  9 * sol0.score)
        {
            continue;
        }
        minScore = min(minScore, sol.score);
    }

    f.writeln("first: ", sol0.score);
    f.writeln("max score: ", maxScore, " rate: ", double(maxScore) / double(sol0.score));
    f.writeln("min score: ", minScore);
    auto diff = maxScore - minScore;

    foreach (cw, sol; cws[1 .. $])
    {
        f.writef("%4d: cl(%4d): cw(%3d): tc(%8d): ea(%8d): sc(%8d): ",
            cw + 1, sol.countL, sol.countW, sol.timeCost, sol.earnings, sol.score
        );
        auto cnt = max(0, 100 * (sol.score - minScore) / diff);
        f.writeln('*'.repeat(cnt).array);
    }
}

class Solution
{
    Worker[] workers;
    int countL, countW, timeCost, earnings, score;
    this(Worker[] ws, int cl, int cw, int tc, int e)
    {
        workers = ws;
        countL = cl;
        countW = cw;
        timeCost = tc;
        earnings = e;
        score = earnings - timeCost - 240 * countW;
    }
}

auto solveG(Problem p, const int maxW, int[] locs = null)
{
    import std.algorithm : max, sort;

    if (locs is null)
    {
        locs = new int[p.locations.length];
        foreach (i, ref v; locs)
        {
            auto loc = p.locations[i];
            v = (loc.endTime - loc.duration) * 10000 + loc.id;
        }
        locs.sort();
    }

    auto workers = new Worker[maxW];
    foreach (i; 0 .. workers.length)
    {
        workers[i] = new Worker;
    }
    auto idle = new bool[workers.length];

    int countL = 0, earnings = 0;

    foreach (v; locs)
    {
        auto startTime = v / 10000;
        auto loc = p.get(v % 10000);
        int cnt = 0;
        foreach (i, w; workers)
        {
            if (w.empty)
            {
                idle[i] = true;
                ++cnt;
            }
            else
            {
                auto cmd = w.back;
                auto dist = loc.pos.distance(p.get(cmd.id).pos);
                if (cmd.time2 + dist <= startTime)
                {
                    idle[i] = true;
                    ++cnt;
                }
                else
                {
                    idle[i] = false;
                }
            }
        }
        if (cnt < loc.require)
        {
            continue;
        }
        ++countL;
        earnings += loc.duration * loc.require * (loc.require + 5);
        auto mem = new int[cnt];
        foreach (i, w; workers)
        {
            if (!idle[i])
            {
                continue;
            }
            auto pos = w.empty ? p.base.pos : p.get(w.back.id).pos;
            auto dist = loc.pos.distance(pos);
            if (w.empty)
            {
                dist += 240;
            }
            --cnt;
            mem[cnt] = dist * workers.length + i;
        }
        mem.sort();
        auto fastTime = 0;
        foreach (i; 0 .. loc.require)
        {
            auto w = workers[mem[i] % $];
            auto time = mem[i] / workers.length;
            if (w.empty)
            {
                time -= 240;
            }
            else
            {
                time += w.back.time2;
            }
            fastTime = max(fastTime, time);
        }
        if (fastTime < startTime)
        {
            startTime = max(fastTime, loc.startTime);
        }
        foreach (i; 0 .. loc.require)
        {
            auto w = workers[mem[i] % $];
            w.insert(Command.arrive(startTime, loc.id));
            w.insert(Command.work(startTime, startTime + loc.duration, loc.id));
        }
    }

    int countW = 0, timeCost = 0;
    foreach (w; workers)
    {
        if (w.empty)
        {
            continue;
        }
        ++countW;
        timeCost -= w.front.time1 - p.base.pos.distance(p.get(w.front.id).pos);
        timeCost += w.back.time2 + p.base.pos.distance(p.get(w.back.id).pos);
    }

    return new Solution(workers, countL, countW, timeCost, earnings);
}

auto solve(Problem p, ref StopWatch sw)
{
    import std.algorithm : max, sort;
    import std.random : uniform, Random;

    auto sol = p.solveG(1000);
    auto maxW = sol.countW * 101 / 100;
    auto minW = sol.countW * 98 / 100;
    auto locs = new int[p.locations.length];
    auto rnd = Random(1983);
    auto iv = 0L;
    auto isw = StopWatch(AutoStart.yes);
    while (true)
    {
        isw.reset;
        foreach (i, ref v; locs)
        {
            auto loc = p.locations[i];
            auto start = uniform(loc.startTime, loc.endTime - loc.duration + 1, rnd);
            v = start * 10000 + loc.id;
        }
        locs.sort();
        auto wc = uniform(minW, maxW, rnd);
        auto tmp = p.solveG(wc, locs);
        if (tmp.score > sol.score)
        {
            sol = tmp;
        }
        iv = max(iv, isw.peek.total!"msecs");
        if (iv + sw.peek.total!"msecs" > 14800)
        {
            break;
        }
    }
    return sol;
}